tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(x={$e1.st},y={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(x={$e1.st},y={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(x={$e1.st},y={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(x={$e1.st},y={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}?  -> setVariable(id={$i1.text},x={$e2.st})
        | INT                      -> int(x={$INT.text})
        | ID                       {globals.hasSymbol($ID.text)}?  -> getVariable(id={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr) {i++} -> if(cond={$e1.st},tr={$e2.st},fa={$e3.st},i={i.toString()})
    ;
    