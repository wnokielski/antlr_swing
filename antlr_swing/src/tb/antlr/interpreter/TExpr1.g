tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = myDivide($e1.out, $e2.out);}
        | ^(PRINT e=expr)         {drukuj ($e.text + " = " + $e.out.toString());}
        | INT                      {$out = getInt($INT.text);}
        ;
        
variable

        : ^(PODST i1=ID   e2=expr) {setVariable($i1.text, $e2.out);}
        | ^(VAR id=ID)             {createVariable($id.text);}
        ;