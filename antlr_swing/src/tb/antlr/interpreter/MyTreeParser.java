package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.DivideByZeroException;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	
	protected LocalSymbols variables = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer myDivide(int x, int y) throws DivideByZeroException {
		if (y != 0) {
			return x/y;
		} else 
			throw new DivideByZeroException("You can't divide by zero!");
	}
	
	protected void setVariable (String name, int value) {
		variables.setSymbol(name, value);
	}
	
	protected int getVariable (String name) {
		return variables.getSymbol(name);
	}
	
	protected void createVariable(String name) {
		variables.newSymbol(name);
	}
}
